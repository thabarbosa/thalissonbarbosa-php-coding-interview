<?php

namespace Src\Services;

class NumverifyClient
{
    private string $baseUrl = 'http://apilayer.net/api/';

    private string $apiKey = 'a5916c632a165b696e3e3009eda59e19';

    protected function get(string $endpoint, array $options): object
    {
        $curl = curl_init();

        $options['access_key'] = $this->apiKey;

        $options = http_build_query($options);

        $url = $this->baseUrl . $endpoint . '?' . $options;

        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url
        ]);

        $request = curl_exec($curl);

        curl_close($curl);

        return json_decode($request);
    }
}