<?php

namespace Src\Services;

class NumverifyValidator extends NumverifyClient
{
    public function validateNumber(string $number): bool
    {
        $options = [
            'number' => $number,
        ];

        $request = $this->get('validate', $options);

        return $request->valid ?? false;
    }
}