<?php

namespace Src\Controllers;

use Src\Helpers\Validator;
use Src\Models\ClientModel;

class Client
{
	private function getClientModel(): ClientModel
    {
		return new ClientModel();
	}

	public function getClients(): array
    {
		return $this->getClientModel()->getClients();
	}

    public function getClientById(int $id): ?array
    {
        return $this->getClientModel()->getClientById($id);
    }

	public function createClient(array $client): array
    {
        $validator = new Validator($client);

        if ($validator->fails()) {
            return $validator->getErrors();
        }

        if ($validation = $this->getClientModel()->emailExists($client['email'] ?? null)) {
            return $validation;
        }

		return $this->getClientModel()->createClient($client);
	}

	public function updateClient(array $client): array
    {
        $validator = new Validator($client);

        if ($validator->fails()) {
            return $validator->getErrors();
        }

        if (!$validation = $this->getClientModel()->idExists($client['id'] ?? null)) {
            return $validation;
        }

        if ($validation = $this->getClientModel()->emailExists($client['email'] ?? null)) {
            return $validation;
        }

		return $this->getClientModel()->updateClient($client);
	}

    public function deleteClient(string|int $clientId): array
    {
        if (!$validation = $this->getClientModel()->idExists($clientId)) {
            return $validation;
        }

        $client = [
            'id' => $clientId,
            'deleted_at' => date(time()),
        ];

        return $this->getClientModel()->updateClient($client);
    }
}