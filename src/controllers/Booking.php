<?php

namespace Src\Controllers;

use Src\Models\BookingModel;
use Src\models\DogModel;

class Booking
{
	private function getBookingModel(): BookingModel
    {
		return new BookingModel();
	}

	public function getBookings(): array
    {
		return $this->getBookingModel()->getBookings();
	}

    public function createBooking(array $data): array
    {
        $hasDiscount = $this->checkDiscount($data['clientid']);

        if ($hasDiscount) {
            $this->applyDiscount($data['price']);
        }

        return $this->getBookingModel()->createBooking($data);
    }

    public function checkDiscount($clientId): bool
    {
        $clientDogs = (new DogModel())->getClientDogs($clientId);

        $clientDogsAges = array_map(fn ($dog) => $dog['age'], $clientDogs);

        $agesAverage = array_sum($clientDogsAges) / count($clientDogsAges);

        return $agesAverage < 10;
    }

    public function applyDiscount(&$price): void
    {
        $price *= 0.9;
    }
}