<?php

namespace Src\Controllers;

use Src\Models\DogModel;

class Dog
{
	private function getDogModel(): DogModel
    {
		return new DogModel();
	}

	public function getDogs(): array
    {
		return $this->getDogModel()->getDogs();
	}
}