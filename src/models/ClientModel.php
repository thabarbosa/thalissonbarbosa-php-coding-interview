<?php

namespace Src\models;

use Src\helpers\Helpers;

class ClientModel
{
	private array $clientData;

	function __construct()
    {
		$string = file_get_contents(dirname(__DIR__, 2) . '/scripts/clients.json');
		$this->clientData = json_decode($string, true);
	}

	public function getClients(): array
    {
		return $this->clientData;
	}

	public function createClient(array $data): array
    {
		$clients = $this->getClients();

		$data['id'] = end($clients)['id'] + 1;
		$clients[] = $data;

		Helpers::putJson($clients, 'clients');

		return $data;
	}

	public function updateClient(array $data): array
    {
		$updateClient = [];
		$clients = $this->getClients();

		foreach ($clients as $key => $client) {
			if ($client['id'] == $data['id']) {
				$clients[$key] = $updateClient = array_merge($client, $data);
			}
		}

		Helpers::putJson($clients, 'clients');

		return $updateClient;
	}

	public function getClientById(string|int $id): ?array
    {
		$clients = $this->getClients();

        return Helpers::searchArray($clients, $id);
	}

	public function getClientByEmail(string $email): ?array
    {
		$clients = $this->getClients();

        return Helpers::searchArray($clients, $email, 'email');
	}


    public function idExists(?int $id): bool|array
    {
        if ($this->getClientById($id)) {
            return [
                'errors' => [
                    'id' => 'This client does not exists',
                ],
            ];
        }

        return true;
    }

    public function emailExists(?string $email): bool|array
    {
        if ($this->getClientByEmail($email)) {
            return [
                'errors' => [
                    'email' => 'This client email already exists',
                ],
            ];
        }

        return false;
    }
}