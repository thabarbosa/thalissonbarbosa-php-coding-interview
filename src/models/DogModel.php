<?php

namespace Src\Models;

class DogModel
{
	private array $dogData;

	function __construct()
    {
		$string = file_get_contents(dirname(__DIR__, 2) . '/scripts/dogs.json');
		$this->dogData = json_decode($string, true);
	}

	public function getDogs(): array
    {
		return $this->dogData;
	}

    public function getClientDogs($clientId): array
    {
        return array_filter($this->dogData, fn ($dog) => $dog['clientid'] === $clientId);
    }
}