<?php

namespace Src\Models;

use Src\Helpers\Helpers;

class BookingModel
{
	private array $bookingData;

	function __construct() {
		$string = file_get_contents(dirname(__DIR__, 2) . '/scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
	}

	public function getBookings(): array
    {
		return $this->bookingData;
	}

    public function createBooking(array $booking): array
    {
        $bookings = $this->getBookings();

        $booking['id'] = end($bookings)['id'] + 1;
        $bookings[] = $booking;

        Helpers::putJson($bookings, 'bookings');

        return $booking;
    }
}