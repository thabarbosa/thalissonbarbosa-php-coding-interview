<?php

namespace Src\Helpers;

class Helpers
{
	public static function putJson(array $users, string $entity): void
    {
		file_put_contents(dirname(__DIR__) . '/../scripts/'. $entity . '.json', json_encode($users, JSON_PRETTY_PRINT));
	}

	public static function arraySearchI($needle, $haystack, $column): string|int|false
    {
		return array_search($needle, array_column($haystack, $column));
	}

    public static function searchArray($haystack, $value, $column = 'id'): ?array
    {
        $data = array_filter($haystack, fn ($data) => $data[$column] == $value);

        return array_shift($data);
    }
}