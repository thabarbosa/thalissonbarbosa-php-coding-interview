<?php

namespace Src\Helpers;

use Src\Services\NumverifyValidator;

class Validator
{
    private array $errors = [];

    public function __construct(array $options)
    {
        foreach ($options as $key => $value) {
            $method = 'validate' . ucwords($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function fails(): bool
    {
        return !empty($this->errors);
    }

    public function getErrors(): array
    {
        return [
            'errors' => $this->errors,
        ];
    }

    private function validatePhone(?string $number): void
    {
        $numverify = new NumverifyValidator();

        $validation = $numverify->validateNumber($number);

        if (!$validation) {
            $this->errors['phone'] = 'Invalid phone number';
        }
    }

    private function validateEmail(?string $email): void
    {
        $validation = filter_var($email, FILTER_VALIDATE_EMAIL);

        if (!$validation) {
            $this->errors['email'] = 'Invalid email';
        }
    }
}