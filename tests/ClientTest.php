<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\Controllers\Client;

class ClientTest extends TestCase
{
	private Client $client;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->client = new Client();
	}

	/** @test */
	public function getClients(): void
    {
		$results = $this->client->getClients();

		$this->assertIsArray($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['username'], 'arojas');
		$this->assertEquals($results[0]['name'], 'Antonio Rojas');
		$this->assertEquals($results[0]['email'], 'arojas@dogeplace.com');
		$this->assertEquals($results[0]['phone'], '1234567');
	}

    /** @test */
	public function createClient(): void
    {
		$client = [
			'username' => 'newuser',
			'name' => 'New User',
			'email' => 'newuser@dogeplace.com',
		];

		$this->client->createClient($client);

        $clients = $this->client->getClients();

        $newClient = $this->client->getClientById(end($clients)['id']);
        $newClient = array_intersect_key($newClient, $client);

		$this->assertIsArray($clients);
        $this->assertEquals($client, $newClient);
	}

    /** @test */
	public function createClientShouldReturnPhoneValidationError(): void
    {
		$client = [
			'username' => 'newuser',
			'name' => 'New User',
			'email' => 'newuser@dogeplace.com',
            'phone' => '123',
		];

		$request = $this->client->createClient($client);

        $clients = $this->client->getClients();

        $lastClient = $this->client->getClientById(end($clients)['id']);
        $lastClient = array_intersect_key($lastClient, $client);

		$this->assertIsArray($clients);
        $this->assertNotEquals($client, $lastClient);
        $this->assertArrayHasKey('phone', $request['errors']);
	}

    /** @test */
	public function createClientShouldReturnEmailValidationError(): void
    {
		$client = [
			'username' => 'newuser',
			'name' => 'New User',
			'email' => 'newuser',
		];

		$request = $this->client->createClient($client);

        $clients = $this->client->getClients();

        $lastClient = $this->client->getClientById(end($clients)['id']);
        $lastClient = array_intersect_key($lastClient, $client);

		$this->assertIsArray($clients);
        $this->assertNotEquals($client, $lastClient);
        $this->assertArrayHasKey('email', $request['errors']);
	}

    /** @test */
	public function createClientShouldReturnEmailExists(): void
    {
		$client = [
			'username' => 'newuser',
			'name' => 'New User',
			'email' => 'arojas@dogeplace.com',
		];

		$request = $this->client->createClient($client);

        $clients = $this->client->getClients();

        $lastClient = $this->client->getClientById(end($clients)['id']);
        $lastClient = array_intersect_key($lastClient, $client);

		$this->assertIsArray($clients);
        $this->assertNotEquals($client, $lastClient);
        $this->assertArrayHasKey('email', $request['errors']);
        $this->assertEquals('This client email already exists', $request['errors']['email']);
	}

    /** @test */
	public function updateClient(): void
    {
		$client = [
			'id' => 3,
			'username' => 'cperez',
			'name' => 'Carlos Perez',
			'email' => 'cperez@dogeplace.com',
		];

		$this->client->updateClient($client);
		$clients = $this->client->getClients();

        $newClient = $this->client->getClientById($client['id']);
        $newClient = array_intersect_key($newClient, $client);

		$this->assertIsArray($clients);
        $this->assertEquals($client, $newClient);
	}

    /** @test */
	public function updateClientShouldReturnEmailExists(): void
    {
		$client = [
			'id' => 3,
			'username' => 'cperez',
			'name' => 'Carlos Perez',
			'email' => 'arojas@dogeplace.com',
		];

        $request = $this->client->updateClient($client);
		$clients = $this->client->getClients();

		$this->assertIsArray($clients);
        $this->assertArrayHasKey('email', $request['errors']);
        $this->assertEquals('This client email already exists', $request['errors']['email']);
	}

    /** @test */
    public function deleteClient(): void
    {
        $clients = $this->client->getClients();

        $clientToDelete = end($clients);

        $this->client->deleteClient($clientToDelete['id']);

        $deletedClient = $this->client->getClientById($clientToDelete['id']);

        $this->assertArrayHasKey('deleted_at', $deletedClient);
        $this->assertNotNull($deletedClient['deleted_at']);
    }
}