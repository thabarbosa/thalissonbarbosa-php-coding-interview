<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\Controllers\Booking;
use Src\Helpers\Helpers;

class BookingTest extends TestCase
{

	private $booking;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void
    {
		parent::setUp();
		$this->booking = new Booking();
	}

	/** @test */
	public function getBookings(): void
    {
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

    /** @test */
    public function createBooking(): void
    {
        $booking = [
            'clientid' => 3,
            'price' => 200,
            'checkindate' => "2021-08-04 15:00:00",
            'checkoutdate' => "2021-08-11 15:00:00",
        ];

        $this->booking->createBooking($booking);

        $results = $this->booking->getBookings();

        $newBooking = Helpers::searchArray($results, 3, 'clientid');
        $newBooking = array_intersect_key($newBooking, $booking);

        $this->assertIsArray($results);
        $this->assertEquals($newBooking, $booking);

    }

    /** @test */
    public function createBookingWithDiscount(): void
    {
        $booking = [
            'clientid' => 1,
            'price' => 200,
            'checkindate' => "2021-08-04 15:00:00",
            'checkoutdate' => "2021-08-11 15:00:00",
        ];

        $newBooking = $this->booking->createBooking($booking);

        $results = $this->booking->getBookings();

        $newBooking = array_intersect_key($newBooking, $booking);

        $this->assertIsArray($results);
        $this->assertEquals(180, $newBooking['price']);
    }
}