<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\Services\NumverifyValidator;

class NumverifyTest extends TestCase
{

    /** @test */
    public function validateNumberShouldReturnFalse(): void
    {
        $verification = (new NumverifyValidator)->validateNumber('123');

        $this->assertFalse($verification);
    }

    /** @test */
    public function validateNumberShouldReturnTrue(): void
    {
        // throtle numverify api
        sleep(1);

        $verification = (new NumverifyValidator)->validateNumber('558695487552');

        $this->assertTrue($verification);
    }
}